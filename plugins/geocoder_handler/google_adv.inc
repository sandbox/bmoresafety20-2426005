<?php

/**
 * @file
 * Plugin to provide a google geocoder with advanced functionality.
 */

$plugin = array(
  'title' => t("Google Geocoder (Advanced)"),
  'description' => t('Geocodes via google geocoder'),
  'callback' => 'geocoder_google_adv',
  'field_types' => array('text', 'text_long', 'addressfield', 'location', 'text_with_summary', 'computed', 'taxonomy_term_reference'),
  'field_callback' => 'geocoder_google_adv_field',
  'settings_callback' => 'geocoder_google_adv_form',
  'terms_of_service' => 'http://code.google.com/apis/maps/documentation/geocoding/#Limits',
);

/**
 * Geocode callback.
 *
 * Attempt to pull the value from cache, then persistent storage, and lastly
 * pass off to the standard Google geocoder for lookup.
 */
function geocoder_google_adv($address, $options = array()) {
  geophp_load();

  // Normalize address.
  $key = geocoder_google_adv_normalize_address($address);

  // Pull from cache if possible.
  if ($cache = cache_get($key, 'cache_geocoder_google_adv')) {
    $data = $cache->data;
    if (is_object($data) && get_class($data) == 'Exception') {
      throw $data;
    }
    return $data;
  }

  // Pull from storage if possible.
  $query = "SELECT data FROM {geocoder_google_adv} WHERE cid = ?";
  if ($result = db_query($query, array($key))->fetchField()) {
    $data = unserialize($result);
    // If it's an exception, we throw it.
    if (is_object($data) && get_class($data) == 'Exception') {
      throw $data;
    }
    return $data;
  }

  // Pass off to standard Google geocoder for lookup.
  try {
    geocoder_google_adv_include();
    $data = geocoder_google($address, $options);
  } catch (Exception $e) {
    // Immediately throw everything except ZERO_RESULTS errors.
    if (strpos($e->getMessage(), 'ZERO_RESULTS') !== FALSE) {
      throw $e;
    }
  }

  // Save the result.
  $record = array(
    'cid' => $key,
    'data' => $data,
  );
  drupal_write_record('geocoder_google_adv', $record);
  cache_set($key, $data, 'cache_geocoder_google_adv');

  // Throw em if you got em.
  if (is_object($data) && get_class($data) == 'Exception') {
    throw $data;
  }

  return $data;
}

/**
 * Field callback.
 *
 * This code processes another field value into a geofield type field. This is
 * copied nearly exactly from geocoder_google_field().
 */
function geocoder_google_adv_field($field, $field_item, $options = array()) {
  if ($field['type'] == 'text' || $field['type'] == 'text_long' || $field['type'] == 'text_with_summary' || $field['type'] == 'computed') {
    return geocoder_google_adv($field_item['value'], $options);
  }
  if ($field['type'] == 'addressfield') {
    $address = geocoder_widget_parse_addressfield($field_item);
    return geocoder_google_adv($address, $options);
  }
  if ($field['type'] == 'location') {
    $address = geocoder_widget_parse_locationfield($field_item);
    return geocoder_google_adv($address, $options);
  }
  if ($field['type'] == 'taxonomy_term_reference') {
    $term = taxonomy_term_load($field_item['tid']);
    return geocoder_google_adv($term->name);
  }
}

/**
 * Settings form callback.
 */
function geocoder_google_adv_form($default_values = array()) {
  geocoder_google_adv_include();
  $form = geocoder_google_form($default_values);
  return $form;
}

/**
 * Utility function to ensure that the Google geocoder is included.
 */
function geocoder_google_adv_include() {
  ctools_include('plugins');
  $plugin = ctools_get_plugins('geocoder', 'geocoder_handler', 'google');
}

/**
 * Utility function to remove abnormalities from addresses.
 *
 * This is to increase the hit rate when attempting to query the data from
 * storage or cache.
 */
function geocoder_google_adv_normalize_address($address) {
  $key = transliteration_get($address);
  // Replace whitespace.
  $key = str_replace(' ', '_', $key);
  // Remove remaining unsafe characters.
  $key = preg_replace('![^0-9A-Za-z_.-]!', '', $key);
  // Remove multiple consecutive non-alphabetical characters.
  $key = preg_replace('/(_)_+|(\.)\.+|(-)-+/', '\\1\\2\\3', $key);
  // Set to lower-case.
  $key = strtolower($key);

  return $key;
}
